// This file can be replaced during build by using the `fileReplacements` array.
// `ng build ---prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
    production: false,
    hmr       : false,
    firebaseConfig : {
        apiKey: "AIzaSyDBEAR6cPTvDe53Ff8ghPTNOYspGwsAgE8",
        authDomain: "ping10.firebaseapp.com",
        databaseURL: "https://ping10.firebaseio.com",
        projectId: "ping10",
        storageBucket: "ping10.appspot.com",
        messagingSenderId: "961480770389",
        appId: "1:961480770389:web:50babc92d412a6120ae5dc",
        measurementId: "G-K1XGJNMXN1"
      },
};

/*
 * In development mode, to ignore zone related error stack frames such as
 * `zone.run`, `zoneDelegate.invokeTask` for easier debugging, you can
 * import the following file, but please comment it out in production mode
 * because it will have performance impact when throw error
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.
