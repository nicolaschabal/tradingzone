export const locale = {
    lang: 'fr',
    data: {
        'NAV': {
            'APPLICATIONS': 'Fonctionnalités',
            'DASHBOARDS'  : 'Dashboards',
            'CALENDAR'    : 'Calendrier',
            'MAIL'        : {
                'TITLE': 'Mail',
                'BADGE': '25'
            },
            'MAIL_NGRX'        : {
                'TITLE': 'Mail Ngrx',
                'BADGE': '13'
            },
            'CHAT'        : 'Discussion',
            'FILE_MANAGER': 'Gestionnaire de fichiers',
            'CONTACTS'    : 'Contacts',
            'TODO'        : 'A faire'
        }
    }
};