
import { Component, OnDestroy, OnInit, ViewEncapsulation, ChangeDetectorRef } from '@angular/core';
import { Subject } from 'rxjs';
import { takeUntil } from 'rxjs/operators';
import { TranslateService } from '@ngx-translate/core';
import * as _ from 'lodash';

import { FuseConfigService } from '@fuse/services/config.service';
import { FuseSidebarService } from '@fuse/components/sidebar/sidebar.service';
import { AuthentificationService } from "../../../main/pages/authentication/authentification.service";

import { navigation } from 'app/navigation/navigation';
import { UserService } from 'app/main/apps/users/user.service';
import { AppComponent } from 'app/app.component';
import { BreakpointObserver, BreakpointState } from '@angular/cdk/layout';

@Component({
    selector     : 'toolbar',
    templateUrl  : './toolbar.component.html',
    styleUrls    : ['./toolbar.component.scss'],
    encapsulation: ViewEncapsulation.None
})

export class ToolbarComponent implements OnInit, OnDestroy
{
    horizontalNavbar: boolean;
    rightNavbar: boolean;
    hiddenNavbar: boolean;
    languages: any;
    navigation: any;
    selectedLanguage: any;
    userStatusOptions: any[];
    firstName: string;
    lastName: string;
    isLarge: boolean;
    // Private
    private _unsubscribeAll: Subject<any>;
    private destroy$ = new Subject();

    /**
     * Constructor
     *
     * @param {FuseConfigService} _fuseConfigService
     * @param {FuseSidebarService} _fuseSidebarService
     * @param {TranslateService} _translateService
     */
    constructor(
        public appComponent : AppComponent,
        public authService: AuthentificationService,
        private _fuseConfigService: FuseConfigService,
        private _fuseSidebarService: FuseSidebarService,
        private _translateService: TranslateService,
        private _UserService: UserService,
        private breakpointObserver: BreakpointObserver,
        private cd: ChangeDetectorRef
        
    )
    {
        // Set the defaults
        this.userStatusOptions = [
            {
                'title': 'Online',
                'icon' : 'icon-checkbox-marked-circle',
                'color': '#4CAF50'
            },
            {
                'title': 'Away',
                'icon' : 'icon-clock',
                'color': '#FFC107'
            },
            {
                'title': 'Do not Disturb',
                'icon' : 'icon-minus-circle',
                'color': '#F44336'
            },
            {
                'title': 'Invisible',
                'icon' : 'icon-checkbox-blank-circle-outline',
                'color': '#BDBDBD'
            },
            {
                'title': 'Offline',
                'icon' : 'icon-checkbox-blank-circle-outline',
                'color': '#616161'
            }
        ];

        this.languages = [
            {
                id   : 'fr',
                title: 'French',
                flag : 'fr'
            },
            {
                id   : 'en',
                title: 'English',
                flag : 'us'
            }
        ];

        this.navigation = navigation;

        // Set the private defaults
        this._unsubscribeAll = new Subject();
    }

    // -----------------------------------------------------------------------------------------------------
    // @ Lifecycle hooks
    // -----------------------------------------------------------------------------------------------------

    /**
     * On init
     */
    ngOnInit(): void
    {
        this.getUserInfo();
        // Subscribe to the config changes
        this._fuseConfigService.config
            .pipe(takeUntil(this._unsubscribeAll))
            .subscribe((settings) => {
                this.horizontalNavbar = settings.layout.navbar.position === 'top';
                this.rightNavbar = settings.layout.navbar.position === 'right';
                this.hiddenNavbar = settings.layout.navbar.hidden === true;
            });

        // Set the selected language from default languages
        this.selectedLanguage = _.find(this.languages, {'id': this._translateService.currentLang});

        const RESPONSIVE = {
            large: {
                breakpoint: '(max-width: 1279px)',
                isLarge: true,
            }
        };

        this.breakpointObserver
            .observe(
                Object.values(RESPONSIVE).map(({ breakpoint }) => breakpoint)
            )
            .pipe(takeUntil(this.destroy$))
            .subscribe((state: BreakpointState) => {
                const foundBreakpoint = Object.values(RESPONSIVE).find(
                    ({ breakpoint }) => !!state.breakpoints[breakpoint]
                );
                if (foundBreakpoint) {
                    this.isLarge = foundBreakpoint.isLarge;
                } else {
                    this.isLarge = false
                }
                this.cd.markForCheck();
            });



    }

    public settings() : void {
        this.appComponent.toggleSidebarOpen('themeOptionsPanel');
    }

    public signOut(): void {
        this.authService.signOut();
    }
    
    getUserInfo():void {
        if(localStorage.getItem('currentUser')) {
            this.firstName = this._UserService.getUserFromLocalStorage().firstName;
            this.lastName = this._UserService.getUserFromLocalStorage().lastName;
        }
    }

    /**
     * On destroy
     */
    ngOnDestroy(): void
    {
        // Unsubscribe from all subscriptions
        this._unsubscribeAll.next();
        this._unsubscribeAll.complete();
    }

    // -----------------------------------------------------------------------------------------------------
    // @ Public methods
    // -----------------------------------------------------------------------------------------------------

    /**
     * Toggle sidebar open
     *
     * @param key
     */
    toggleSidebarOpen(key): void
    {
        this._fuseSidebarService.getSidebar(key).toggleOpen();
    }

    /**
     * Search
     *
     * @param value
     */
    search(value): void
    {
        // Do your search here...
        console.log(value);
    }

    /**
     * Set the language
     *
     * @param lang
     */
    setLanguage(lang): void
    {
        // Set the selected language for the toolbar
        this.selectedLanguage = lang;

        // Use the selected language for translations
        this._translateService.use(lang.id);
    }
}
