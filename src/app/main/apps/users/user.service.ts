import { Observable } from 'rxjs';
import { Injectable } from '@angular/core';
import { User } from './user'
import { AngularFirestoreCollection, AngularFirestore, AngularFirestoreDocument } from '@angular/fire/firestore';
import { map } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class UserService {

  users: Observable<User[]>;
  user: Observable<User>;
  userCollection: AngularFirestoreCollection<User>;
  pictureUrl: string;

  constructor(private firestore: AngularFirestore) {
    this.userCollection = firestore.collection<User>('users');
  }

  /**
   * @param user 
   * @description Permet de stocké les données de firebase concernant le user connecté dans le local storage
   */
  public setCurrentUserCredential(userCredential: firebase.User) {
    localStorage.setItem('userCredential', JSON.stringify(userCredential));
  }

  /**
   * @param userId 
   * @description Permet de set le user stocké en base avec ses propriétés directement dans le localStorage
   */
  public setCurrentUser(userId: string) {
    this.getUser(userId).subscribe((user) => {
      localStorage.setItem('currentUser', JSON.stringify(user))
    });
  }

  /**
   * @description Retourne le user depuis le localStorage
   */
  public getUserFromLocalStorage(): User {
    const user: User = JSON.parse(localStorage.getItem('currentUser')) as User;
    if (localStorage.getItem('currentUser')) {
      return user
    }
  } 

  /**
   * @param userId 
   * @description Permet de chercher un User selon son ID dans firebase
   */
  public getUser(userId: string) {
    return this.firestore.doc<User>('users/' + userId).valueChanges();
  }

    /**
   * @param userId 
   * @description Permet de chercher un User selon son ID dans firebase avec moins de propriétés
   */
  public getLightUser(userId: string) {
   return this.firestore.doc<User>('users/' + userId).valueChanges().pipe(
      map((user) => {
        let lightUser = {
          firstName: user.firstName,
          lastName: user.lastName,
          birthday: user.birthday,
          phone: user.phone,
          company: user.company,
          job: user.job,
        }
        return lightUser
      })
    )
  }

  getUserPictureUrl(userId: string) {
    return  this.firestore.doc<User>('users/' + userId).valueChanges().pipe(
      map((res) => {
        return this.pictureUrl = res.pictureUrl
      })
    )
  }

  /**
   * @description Retourne l'ensemble des utilisateurs de Firebase
   */
  public getUsers() {
    return this.users = this.userCollection.valueChanges();
  }


  /**
   * @param user
   * @description Permet de mettre à jour un utilisateur 
   */
  public updateUser(user: User) {
    const userId = this.getUserFromLocalStorage().uid;
    let updatedUser = {
      firstName: user.firstName,
      lastName: user.lastName,
      birthday: user.birthday,
      phone: user.phone,
      company: user.company,
      job: user.job
    }
    return this.firestore.doc('users/' + userId).update(updatedUser);
  }

  /**
   * @param userId
   * @description Supprime un utilisateur selon son id 
   */
  public deleteUser(userId: string) {
    this.firestore.doc('users/' + userId).delete();
  }

  /**
   * @description Permet de supprimer le localstorage lors de la deconnexion
   */
  public clearLocalStorage() {
    localStorage.clear();
  }
}
