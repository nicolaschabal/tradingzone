export const locale = {
    lang: 'fr',
    data: {
        'CHAT': {
            'ONLINE'                    : 'En ligne',
            'DO_NOT_DISTURB'            : 'Ne pas déranger',
            'AWAY'                      : 'Absent',
            'OFFLINE'                   : 'Hors ligne',
            'PROFILE'                   :'PROFIL',
            'LOGOUT'                    :'DECONNEXION',
            'MOOD'                      :'Humeur',
            'CONTACTS'                  :'Contacts',
            'SEARCH_OR_START'           :'Rechercher ou commencer une nouvelle conversation',
            'SELECT_A_CONTACT'          :"Selectionner un contact pour démarrer une nouvelle conversation !",
            'CHAT'                      :"Discussion",
            'CHATS'                     :"Discussions",
            "TYPE_YOUR_MSG"             :"Taper votre message...",
            "CONTACT_INFO"              :"Information sur le contact",
            "CHAT_APP"                  :"Application de discussion",
            "NO_RESULTS"                :"Pas de résultats...",
        }
    }
};