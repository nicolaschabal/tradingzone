export const locale = {
    lang: 'en',
    data: {
        'CHAT': {
            'ONLINE'                    : 'ONLINE',
            'DO_NOT_DISTURB'            : 'Do not disturb',
            'AWAY'                      : 'Away',
            'OFFLINE'                   : 'Offline',
            'PROFILE'                   :'PROFILE',
            'LOGOUT'                    :'LOGOUT',
            'MOOD'                      :'Mood',
            'CONTACTS'                  :'Contacts',
            'SEARCH_OR_START'           :'Search or start new chat',
            'SELECT_A_CONTACT'          :"Select a contact to start a chat!",
            'CHAT'                      :"Chat",
            'CHATS'                     :"Chats",
            "TYPE_YOUR_MSG"             :"Type your message...",
            "CONTACT_INFO"              :"Contact Info",
            "CHAT_APP"                  :"Chat App",
            "NO_RESULTS"                :"No results...",

        }
    }
};
