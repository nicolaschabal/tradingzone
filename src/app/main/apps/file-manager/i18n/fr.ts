export const locale = {
    lang: 'fr',
    data: {
        'FILE_MANAGER': {
            'NAME'                      :'Nom',
            'INFO'                      :'Informations',
            'TYPE'                      :'Type',
            'SIZE'                      :'Taille',
            'LOCATION'                  :'Localisation',
            'OWNER'                     :'Propriétaire',
            'MODIFIED'                  :'Modifié',
            'OPENED'                    :'Ouvert',
            'CREATED'                   :'Crée',
            'FILE_MANAGER'              :'Gestionnaire de Ficher',
            'MY_FILE'                   :'Mes fichiers',
            'SHARED_WITH_ME'            :'Partagé avec moi',
            'NOT_INTERESTED'            :'Non interessé',
            'OFFLINE'                   :'Hors-ligne',
            'RECENT'                    :'Récent',
            'AVAILABLE_OFFLINE'         :'Disponible Hors-ligne',
            'STARRED'                   :'Favoris',
            'EDITED'                    :'Edition',
        }
    }
};
