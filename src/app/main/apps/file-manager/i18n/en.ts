export const locale = {
    lang: 'en',
    data: {
        'FILE_MANAGER': {
            'NAME'                      :'Name',
            'INFO'                      :'Info',
            'TYPE'                      :'Type',
            'SIZE'                      :'Size',
            'LOCATION'                  :'Location',
            'OWNER'                     :'Owner',
            'MODIFIED'                  :'Modified',
            'OPENED'                    :'Opened',
            'CREATED'                   :'Created',
            'FILE_MANAGER'              :'File Manager',
            'MY_FILE'                   :'My file',
            'SHARED_WITH_ME'            :'Shared with me',
            'NOT_INTERESTED'            :'Not interested',
            'OFFLINE'                   :'Offline',
            'RECENT'                    :'Recent',
            'AVAILABLE_OFFLINE'         :'Available Offline',
            'STARRED'                   :'Starred',
            'EDITED'                    :'Edited',
        }
    }
};
