export const locale = {
    lang: 'en',
    data: {
        'TODO': {
            'TO_DO'                     :'To do',
            'ALL'                       :'All',
            'NONE'                      :'None',
            'READ'                      :'Read',
            'UNREAD'                    :'Unread',
            'STARRED'                   :'Starred',
            'UNSTARRED'                 :'Unstarred',
            'IMPORTANT'                 :'Important',
            'UNIMPORTANT'               :'Unimportant',
            'ADD_TASK'                  :'ADD TASK',
            'FILTERS'                   :'FILTERS',
            'TAGS'                      :'TAGS',
            'SELECT_A_TODO'             :'Select a ToDo',
            'MARK_AS_DONE'              :'Mark as done',
            'TITLE'                     :'Title',
            'START_DATE'                :'Start date',
            'DUE_DATE'                  :'Due date',
            'NOTES'                     :'Notes',
            'SAVE'                      :'SAVE',
            'SEARCH_FOR_A_TASK'         :'Search for a task',
            'MARK_AS_UNDONE'            :'Mark as undone',
            'MARK_AS_IMPORTANT'         :'Mark as important',
            'REMOVE_STAR'               :'Remove star',
            'ADD_STAR'                  :'Add star',
            'REMOVE_IMPORTANT'          :'Remove important',
            'THERE_ARE_NO_TODO'         :'There are no Todo!',
        }
    }
};
