export const locale = {
    lang: 'fr',
    data: {
        'TODO': {
            'TO_DO'                     :'À faire',
            'ALL'                       :'Tous',
            'NONE'                      :'Aucun',
            'READ'                      :'Lu',
            'UNREAD'                    :'Non lu',
            'STARRED'                   :'Favoris',
            'UNSTARRED'                 :'Non favoris',
            'IMPORTANT'                 :'Important',
            'UNIMPORTANT'               :'Non important',
            'ADD_TASK'                  :'AJOUTER UNE TÂCHE',
            'FILTERS'                   :'FILTRES',
            'TAGS'                      :'TAGS',
            'SELECT_A_TODO'             :'Selectionner une tâche à faire',
            'MARK_AS_DONE'              :'Tâche Terminée',
            'TITLE'                     :'Titre',
            'START_DATE'                :'Date de départ',
            'DUE_DATE'                  :'Date de fin',
            'NOTES'                     :'Notes',
            'SAVE'                      :'SAUVEGARDER',
            'SEARCH_FOR_A_TASK'         :'Rechercher une tâche',
            'MARK_AS_UNDONE'            :'Tâche non terminée',
            'MARK_AS_IMPORTANT'         :'Ajouter aux importants',
            'REMOVE_IMPORTANT'          :'Enlever des importants',
            'REMOVE_STAR'               :'Enlever des favoris',
            'ADD_STAR'                  :'Ajouter aux favoris',
            'THERE_ARE_NO_TODO'         :'Pas de tâches à faire!',
        }
    }
};