import { map } from 'rxjs/operators';
import { Observable } from 'rxjs';
import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { News, Articles } from './dashboard/News';

@Injectable({
  providedIn: 'root'
})
export class NewsService {

  newsUrl = 'https://newsapi.org/v2/top-headlines?country=fr&category=business&apiKey=5c8f338459d0446683ef6950e5576124'
  constructor(private http: HttpClient) { }


  getNews(): Observable<News[]> {
    return this.http.get(this.newsUrl).pipe(
      map((news: Articles) => {
        return news.articles;
      })
    ) as Observable<News[]>;
  } 

  getNewsDescription(): Observable<string[]> {
    return this.http.get(this.newsUrl).pipe(
      map((news: Articles) => {
        return news.articles.map(a => a.description);
      })
    ) as Observable<string[]>;
  }
}
