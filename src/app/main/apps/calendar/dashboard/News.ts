export class Articles {
  status: string;
  totalResults: string;
  articles:News[]
}
export class News {
  source: {
    id: string;
    name: string;
  };
  author: string;
  title: string;
  description: string;
  url: string;
  urlToImage: string;
  publishedAt: string;
  content: string;
}
