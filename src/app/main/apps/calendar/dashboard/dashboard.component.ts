import { select } from '@ngrx/store';

import { User } from './../../users/user';
import { Component, OnInit, ViewEncapsulation, ChangeDetectorRef, ViewChild, Pipe, ElementRef } from '@angular/core';
import { MatDialog, MatDialogRef } from '@angular/material';
import { MatProgressSpinnerModule } from '@angular/material/progress-spinner';
import { MatSnackBar } from '@angular/material/snack-bar';

import { MatPaginator } from '@angular/material/paginator';
import { Subject, Observable, BehaviorSubject, forkJoin } from 'rxjs';
import { DateFormatterParams, CalendarDateFormatter } from 'angular-calendar'
import { UserService } from "../../users/user.service";
import { fuseAnimations } from '@fuse/animations';
import { FuseTranslationLoaderService } from '@fuse/services/translation-loader.service';

import { MatSort } from '@angular/material/sort';
import { MatTableDataSource } from '@angular/material/table';

import { CompanyService } from 'app/main/apps/calendar/company.service';
import { DatePipe } from '@angular/common';

import { News } from './News';
import { NewsService } from '../news.service';

import { locale as english } from 'app/main/apps/calendar/i18n/en';
import { locale as french } from 'app/main/apps/calendar/i18n/fr';
import { TranslateService } from '@ngx-translate/core';
import { BreakpointObserver, BreakpointState } from '@angular/cdk/layout';
import { takeUntil, map, startWith, filter } from 'rxjs/operators';
import { FormControl } from '@angular/forms';
import { resolve } from 'url';

import { MAT_DIALOG_DATA } from '@angular/material/dialog';


export interface State {
    flag: string;
    name: string;
    population: string;
}

@Component({
    selector: 'app-dashboard',
    templateUrl: './dashboard.component.html',
    styleUrls: ['./dashboard.component.scss'],
    encapsulation: ViewEncapsulation.None,
    animations: fuseAnimations,
    providers: [
        {
            provide: CalendarDateFormatter,
        }
    ]
})

export class DashboardComponent implements OnInit {
    states: State[] = [
        {
            name: 'Arkansas',
            population: '2.978M',
            // https://commons.wikimedia.org/wiki/File:Flag_of_Arkansas.svg
            flag: 'https://upload.wikimedia.org/wikipedia/commons/9/9d/Flag_of_Arkansas.svg'
        },
        {
            name: 'California',
            population: '39.14M',
            // https://commons.wikimedia.org/wiki/File:Flag_of_California.svg
            flag: 'https://upload.wikimedia.org/wikipedia/commons/0/01/Flag_of_California.svg'
        },
        {
            name: 'Florida',
            population: '20.27M',
            // https://commons.wikimedia.org/wiki/File:Flag_of_Florida.svg
            flag: 'https://upload.wikimedia.org/wikipedia/commons/f/f7/Flag_of_Florida.svg'
        },
        {
            name: 'Texas',
            population: '27.47M',
            // https://commons.wikimedia.org/wiki/File:Flag_of_Texas.svg
            flag: 'https://upload.wikimedia.org/wikipedia/commons/f/f7/Flag_of_Texas.svg'
        }
    ];


    displayedColumns2: string[] = ['position', 'name', 'weight', 'symbol'];
    dataSource2 = new MatTableDataSource<PeriodicElement>(ELEMENT_DATA);
    durationInSeconds = 5;

    @ViewChild(MatPaginator) paginator: MatPaginator;
    // @ViewChild(MatProgressSpinnerModule) progressSpiner: MatProgressSpinnerModule;

    @ViewChild(MatSort) sort: MatSort;

    articles: News[] = [];

    // Utilisateur
    userId: string;
    firstName$: Observable<string>;
    lastName$: Observable<string>;
    pictureUrl$: Observable<string>;

    // Datatable
    displayedColumns: string[] = ['avatar', 'name', 'buttons'];
    dataSource: MatTableDataSource<User>;
    contacts: Observable<User[]>;
    companies: any;
    isFilterApplieds: boolean = false;

    private isFilterApplied = new BehaviorSubject<boolean>(false);
    public isFilterApplied$ = this.isFilterApplied.asObservable();


    // Calculs et affichages
    cherche = false;
    avis = {
        vendre: false,
        rien: false,
        acheter: false,
    }
    score = {
        var: {
            ponderation: 0.05,
            valeur: 0
        },
        random: {
            ponderation: 0.05,
            valeur: 0
        },
        finance: {
            ponderation: 0.1,
            valeur: 0
        },
        mm: {
            ponderation: 0.2,
            valeur: 0
        },
        res: {
            ponderation: 0.3,
            valeur: 0
        },
        bbands: {
            ponderation: 0.1,
            valeur: 0
        },
        stoch: {
            ponderation: 0.2,
            valeur: 0
        },
    }
    texte: String
    global = {
        open: 0,
        high: 0,
        low: 0,
        price: 0,
        volume: 0,
        change: 0,
        cloture: 0
    }



    isSmall: boolean = false;

    selectedCompany: any;

    private destroy$ = new Subject();

    stateCtrl = new FormControl();
    filteredStates: Observable<News[]>;

    constructor(
        private _userService: UserService,
        private _newsService: NewsService,
        private _companyService: CompanyService,
        private _fuseTranslationLoaderService: FuseTranslationLoaderService,
        private breakpointObserver: BreakpointObserver,
        private _snackBar: MatSnackBar,
        private cd: ChangeDetectorRef,
        public dialog: MatDialog
    ) {
        // Load the translations
        this._fuseTranslationLoaderService.loadTranslations(english, french);

    }
    ngAfterViewInit() {
        console.log("ici ici");

    }


    openDialog() {
        const dialogRef = this.dialog.open(DialogOverviewExampleDialog);

        dialogRef.afterClosed().subscribe(result => {
            console.log(`Dialog result: ${result}`);
        });
    }

    /**
     * On init
     */
    ngOnInit(): void {
        forkJoin(
            this.stateCtrl.valueChanges,
            this.isFilterApplied$,
          )
          .subscribe(([res1, res2]) => {
    console.log(res2);
    
          });
        this.stateCtrl.valueChanges.subscribe((res) => {
            console.log("1 " + res);
            if(res === null) {
                this.isFilterApplieds = false;

            }
            else {
                this.isFilterApplieds = true;
            }
            console.log("1 " + this.isFilterApplieds);
            
        })
        //this.stateCtrl.setValue('null');
 
        console.log(this.stateCtrl.value);
        
       /* this.stateCtrl.valueChanges.subscribe((x) => {
            this.stateCtrl.setValue('null');
            console.log("qsdqsdqsdqsd" + x);
            
            x !== 'null' ? this.isFilterApplied.next(true) : this.isFilterApplied.next(false);
        });
*/
        /** Permet de récupérer les actualités */
        this.getNews();

        /** Permet de stocker les company dans un tableau */
        this.companies = [
            {
                name: 'MICROSOFT - PAR DEFAUT API',
                symbol: 'MSFT',
                PER: 26.5,
                BNPA: 5.06 * 1.01,
                VBNPA: 30,
                rendement_17: 2.26,
                rendement_18: 1.70,
                urlChart: 'https://api.stockdio.com/visualization/financial/charts/v1/HistoricalPrices?app-key=0478656265C542CCA65478E024966D17&stockExchange=PEX&symbol=KORI&dividends=true&splits=true&culture=French-France&palette=Aurora',
                logoUrl: 'data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAOEAAADhCAMAAAAJbSJIAAAARVBMVEXwUSV/u0IyoNr9uBP////vRgz4u7HK4bd4uDWz1u7+4K4gnNj9tQD4uK3I4LSw1O7vOgBytSX84d3n8d/e7ff/8dwAl9fP6I05AAABIUlEQVR4nO3PRw7CQBAAwQGcjQPx/0/lynLkMNJa1R9oVURSbdd81w+npLKAhISEhISEhISEhISEhISEhISEhISEhISEhISEhISEhISEhISEhISEhISEhISEhISEhISEhISEhISEhISEhISEhISEhISEhISEhISEhISEhISEhISEhISEhISHFLZJ7UshXG9DUtEltdwL4WPtk4rm6BHWH2H9EdYfYf0R1h9h/cWY1PYsvq8pq5iTem+FcLpmFeek5vFHeEmKkJCQkJCQkJCQkJCQkJCQkJCQkJCQkJCQkJCQkJCQkJCQkJCQkJCQkJCQkJCQkJCQkJCQkJCQkJCQkJCQkJCQkJCQkJCQkJCQkJCQkJCQkJCQkJCQkJCQkJDw/z45r46NoCOtpQAAAABJRU5ErkJggg=='
            },
            {
                name: 'KORIAN',
                symbol: 'KORI.PAR',
                PER: 25.49,
                BNPA: 1.53,
                VBNPA: 30,
                rendement_17: 1.54,
                rendement_18: 1.54,
                logoUrl: 'https://www.rt78.fr/sites/default/files/styles/article/public/media/images/korian.jpg?itok=bU25zRc0',
                urlChart: 'https://api.stockdio.com/visualization/financial/charts/v1/HistoricalPrices?app-key=0478656265C542CCA65478E024966D17&stockExchange=PEX&symbol=KORI&dividends=true&splits=true&culture=French-France&palette=Aurora'
            },
            {
                name: 'L\'ORÉAL',
                symbol: 'LOR.STG',
                PER: 36.07,
                BNPA: 7.08,
                VBNPA: 6,
                rendement_17: 1.92,
                rendement_18: 2.91,
                logoUrl: 'https://fontmeme.com/images/loreal-logo-font.png',
                urlChart: ''
            },
            {
                name: 'TECNIPFMC',
                symbol: 'FTI.PAR',
                PER: 24.08,
                BNPA: 0.72,
                VBNPA: -31,
                rendement_17: 0.83,
                rendement_18: 2.64,
                logoUrl: 'https://media.glassdoor.com/sqll/1558212/technipfmc-squarelogo-1487689015437.png',
                urlChart: 'https://api.stockdio.com/visualization/financial/charts/v1/HistoricalPrices?app-key=0478656265C542CCA65478E024966D17&stockExchange=PEX&symbol=KORI&dividends=true&splits=true&culture=French-France&palette=Aurora'
            },
            {
                name: 'ATOS',
                symbol: 'ATO.PAR',
                PER: 11.41,
                BNPA: 6.51,
                VBNPA: -21,
                rendement_17: 1.4,
                rendement_18: 1.78,
                logoUrl: 'https://media.glassdoor.com/sqll/10686/atos-squarelogo-1505719229567.png',
                urlChart: 'https://api.stockdio.com/visualization/financial/charts/v1/HistoricalPrices?app-key=0478656265C542CCA65478E024966D17&stockExchange=PEX&symbol=KORI&dividends=true&splits=true&culture=French-France&palette=Aurora'
            },
            {
                name: 'BIOMÉRIEUX ',
                symbol: 'BIM.PAR',
                logoUrl: 'https://upload.wikimedia.org/wikipedia/commons/7/79/BioMerieux18x600.png',
                PER: 36.75,
                BNPA: 2.17,
                VBNPA: 7,
                rendement_17: 0.46,
                rendement_18: 0.61,
                urlChart: 'https://api.stockdio.com/visualization/financial/charts/v1/HistoricalPrices?app-key=0478656265C542CCA65478E024966D17&stockExchange=PEX&symbol=KORI&dividends=true&splits=true&culture=French-France&palette=Aurora'
            },
            {
                name: 'SOPRA STERIA',
                symbol: 'SOP.PAR',
                logoUrl: 'https://www.solutions-numeriques.com/wp-content/uploads/2016/10/sopra-steria1.png',
                PER: 18.18,
                BNPA: 7.43,
                VBNPA: -25,
                rendement_17: 1.54,
                rendement_18: 2.29,
                urlChart: 'https://api.stockdio.com/visualization/financial/charts/v1/HistoricalPrices?app-key=0478656265C542CCA65478E024966D17&stockExchange=PEX&symbol=KORI&dividends=true&splits=true&culture=French-France&palette=Aurora'
            },
        ]

        this.selectedCompany = this.companies[0]

        this.dataSource2.paginator = this.paginator;

        /** Rendre les donées dispo pour la datatabe */
        this.dataSource = new MatTableDataSource(this.companies);
        this.dataSource.sort = this.sort;

        /** Permet de récupérer l'id stocké dans le local storage */
        this.userId = this._userService.getUserFromLocalStorage().uid;
        this.getUserInfo();


        // this.showConfig(this.selectedCompany.symbol)


        /** Permet de gérer une vue mobile */
        const RESPONSIVE = {
            small: {
                breakpoint: '(max-width: 576px)',
                isSmall: true,
            },
            medium: {
                breakpoint: '(max-width: 768px)',
                isSmall: false,
            },
            large: {
                breakpoint: '(max-width: 960px)',
                isSmall: false,
            }
        };
        this.breakpointObserver
            .observe(
                Object.values(RESPONSIVE).map(({ breakpoint }) => breakpoint)
            )
            .pipe(takeUntil(this.destroy$))
            .subscribe((state: BreakpointState) => {
                const foundBreakpoint = Object.values(RESPONSIVE).find(
                    ({ breakpoint }) => !!state.breakpoints[breakpoint]
                );
                if (foundBreakpoint) {
                    this.isSmall = foundBreakpoint.isSmall;
                } else {
                    this.isSmall = false
                }
                this.cd.markForCheck();
            });

      
        this.filteredStates = this.stateCtrl.valueChanges
            .pipe(
                startWith(''),
                map(article => article ? this._filterStates(article) : this.articles.slice())
            );
    }

    private _filterStates(value: string): News[] {
        const filterValue = value.toLowerCase();
        return this.articles.filter(state => state.title.toLowerCase().indexOf(filterValue) !== -1)
    }


    /** Permet de récupérer les actualités */
    public getNews() {
        this._newsService.getNews().subscribe((news) => {
            this.articles = news
        });
    }


    /** Permet de récupérer l'entreprise en cours */
    public getSelectedCompany(contact): void {
        this.selectedCompany = contact
        this.showConfig(contact.symbol);
    }

    public openToast(message: string) {
        this._snackBar.open(message, '', {
            duration: this.durationInSeconds * 1000,
        });
    }

    private selectedCompanyGraph: String

    public showConfig(symbol: String) {
        // Spinner
        this.cherche = true;
        this.selectedCompanyGraph = symbol
        //Textes
        setTimeout(() => {
            this.texte = "Calcul des time series Daily ..."
            setTimeout(() => {
                this.texte = "Utilisation des données et prise en compte de l'indicateur de Bollinger ..."
                setTimeout(() => {
                    this.texte = "Prise en compte de l'actualité ..."

                    setTimeout(() => {
                        this.bands()
                        // this.resultats()
                    }, 500);
                }, 500);
            }, 500);
        }, 500);



        this.globalQuote()
        // //  this.timeSeries()
        // this.bands()
        // //  this.timeSeries()
        // this.stochs()
        // this.moyenneMobile()
        // this.analyseFinanciere()


    }

    public globalQuote() {
        // this._companyService.getDatas(this.selectedCompany.symbol).subscribe((data:any)=>{
        //     this.global.open = data["data"]['prices']['values'][0][1]
        //     this.global.high = data["data"]['prices']['values'][0][2]
        //     this.global.low = data["data"]['prices']['values'][0][3]
        //     this.global.cloture = data["data"]['prices']['values'][0][4]
        //     this.global.volume = data["data"]['prices']['values'][0][5]
        // })
        // this._companyService.getLatestPrice(this.selectedCompany.symbol).subscribe((data:any)=>{
        //     console.log(data)
        //     this.global.price = data["data"]['prices']['values'][0][2]
        // })

        this._companyService.getGlobalQuote(this.selectedCompany.symbol).subscribe((data: any) => {
            console.log(data)
            if (data['Global Quote']) {
                let quote = data['Global Quote']
                this.selectedCompany.quotes = quote;
                let variation = parseFloat(quote['10. change percent'].replace("/[%]/g", ""));
                // let variation = (100 * (this.global.price - this.global.cloture ) / this.global.cloture).toFixed(2);
                console.log(variation)
                this.global.open = quote['02. open']
                this.global.high = quote['03. high']
                this.global.low = quote['04. low']
                this.global.price = quote['05. price']
                this.global.volume = quote['06. volume']
                this.global.cloture = quote['08. previous close']
                this.global.change = variation

                // alert(parseFloat(this.global.price + ""))
                // alert(parseFloat((this.global.high*0.999) + ""))
                console.log(this.global)

                if (parseFloat(this.global.price + "") > (parseFloat((this.global.high * 0.999) + ""))) {
                    this.score.res.valeur = 0
                }
                else {
                    if (parseFloat(this.global.price + "") < (parseFloat((this.global.low * 1.001) + ""))) {
                        this.score.res.valeur = 100
                    }
                    else {
                        this.score.res.valeur = 100 * (parseFloat(this.global.high + "") - parseFloat(this.global.price + "")) / (parseFloat(this.global.high + "") - parseFloat(this.global.low + ""))
                    }

                }

                if ((variation) >= 2) {
                    this.score.var.valeur = 100;
                }
                if ((variation) >= 1) {
                    this.score.var.valeur = 80;
                }
                if ((variation) >= 0) {
                    this.score.var.valeur = 60;
                }
                if ((variation) <= -2) {
                    this.score.var.valeur = 0;
                }
                if ((variation) <= 1) {
                    this.score.var.valeur = 20;
                }
                if ((variation) < 0) {
                    this.score.var.valeur = 40;
                }

            }
            else {

                this.openToast("Utilisation de l'API de secours")
                // this.selectedCompany = this.companies[0]
                this._companyService.getSecoursGlobalQuote(this.selectedCompany.symbol).subscribe((data: any) => {
                    console.log(data)
                    let quote = data['Global Quote']
                    this.selectedCompany.quotes = quote;
                    let variation = parseFloat(quote['10. change percent'].replace("/[%]/g", ""));
                    console.log(variation)
                    this.global.open = quote['02. open']
                    this.global.high = quote['03. high']
                    this.global.low = quote['04. low']
                    this.global.price = quote['05. price']
                    this.global.volume = quote['06. volume']
                    this.global.cloture = quote['08. previous close']
                    this.global.change = variation

                    if ((variation) >= 2) {
                        this.score.var.valeur = 100;
                    }
                    if ((variation) >= 1) {
                        this.score.var.valeur = 80;
                    }
                    if ((variation) >= 0) {
                        this.score.var.valeur = 60;
                    }
                    if ((variation) <= -2) {
                        this.score.var.valeur = 0;
                    }
                    if ((variation) <= 1) {
                        this.score.var.valeur = 20;
                    }
                    if ((variation) < 0) {
                        this.score.var.valeur = 40;
                    }

                })
                console.log(data)
            }
        }, (error: any) => {
            console.log(error)
        }, () => {
            console.log('Utilisation de Quotes')
        });
    }

    public bands() {

        this._companyService.getBbands(this.selectedCompany.symbol).subscribe((data) => {
            let total = 0
            let actual = {}
            // let data2 = {}
            let ecartActuel, ecartMin, score;
            let ecarts = []
            let tend = 0
            if (data['Technical Analysis: BBANDS']) {
                for (let [key, value] of Object.entries(data['Technical Analysis: BBANDS'])) {

                    if (total == 0) {
                        ecartActuel = value['Real Upper Band'] - value['Real Lower Band']
                        tend = value['Real Middle Band']
                    }
                    else if (total == 1) {
                        tend = tend - value['Real Middle Band']
                        ecarts.push(parseFloat(value['Real Upper Band']) - parseFloat(value['Real Lower Band']))
                    }
                    else {
                        ecarts.push(parseFloat(value['Real Upper Band']) - parseFloat(value['Real Lower Band']))
                    }
                    if (total >= 32)
                        break
                    total++;
                }
            }
            else {

                this.openToast("Utilisation de l'API de secours")

                // this.selectedCompany = this.companies[0]


                this._companyService.getSecoursBbands(this.selectedCompany.symbol).subscribe((data) => {
                    for (let [key, value] of Object.entries(data['Technical Analysis: BBANDS'])) {

                        if (total == 0) {
                            ecartActuel = value['Real Upper Band'] - value['Real Lower Band']
                            tend = value['Real Middle Band']
                        }
                        else if (total == 1) {
                            tend = tend - value['Real Middle Band']
                            ecarts.push(parseFloat(value['Real Upper Band']) - parseFloat(value['Real Lower Band']))
                        }
                        else {
                            ecarts.push(parseFloat(value['Real Upper Band']) - parseFloat(value['Real Lower Band']))
                        }
                        if (total >= 32)
                            break
                        total++;
                    }
                })

            }

            ecartMin = Math.min(...ecarts)

            if (ecartMin < ecartActuel) {
                score = 100 * ecartMin / ecartActuel
            }
            else {
                score = 100
            }

            if (tend > 0) {
                score = 100 - score
            }

            this.score.bbands.valeur = score

        }, (error) => {
            console.log(error)
        }, () => {
            console.log('Utilisation de Bands')
            
            this.stochs()
        });
    }

    public timeSeries() {
        this._companyService.getTimeSeriesDaily(this.selectedCompany.symbol).subscribe((data: any) => {
            if (data['Time Series (Daily)']) {

                this.selectedCompany.times = JSON.stringify(data['Time Series (Daily)'])
            }
            else {
                this._companyService.getSecoursTimeSeriesDaily(this.selectedCompany.symbol).subscribe((data: any) => {
                    this.selectedCompany.times = JSON.stringify(data['Time Series (Daily)'])
                })
            }
        });
    }

    public stochs() {
        let vals = []
        let tend = 0
        let score = 0
        this._companyService.getStochs(this.selectedCompany.symbol).subscribe((data: any) => {
            console.log(data)

            if (data['Technical Analysis: STOCH']) {
                for (let [key, value] of Object.entries(data['Technical Analysis: STOCH'])) {
                    vals.push(value)
                    if (vals.length > 2)
                        break
                }
            }
            else {

                this.openToast("Utilisation de l'API de secours")
                // this.selectedCompany = this.companies[0]
                this._companyService.getSecoursStochs().subscribe((data: any) => {
                    console.log(data)

                    for (let [key, value] of Object.entries(data['Technical Analysis: STOCH'])) {
                        vals.push(value)
                        if (vals.length > 2)
                            break
                    }
                }, (error) => {

                }, () => {
                    tend = (Math.sqrt(vals[1] / vals[2]) - 1) * 100

                    if (tend > 0 && (vals[0] > 18 && vals[0] < 22)) {
                        score = 100
                    }
                    else if (tend < 0 && (vals[0] > 78 && vals[0] < 82)) {
                        score = 0
                    }
                    else {
                        score = 50
                    }

                    this.score.stoch.valeur = score

                    // this.resultats()
                })
            }
        }, (error) => {

        }, () => {
            tend = (Math.sqrt(vals[1] / vals[2]) - 1) * 100

            if (tend > 0 && (vals[0] > 18 && vals[0] < 22)) {
                score = 100
            }
            else if (tend < 0 && (vals[0] > 78 && vals[0] < 82)) {
                score = 0
            }
            else {
                score = 50
            }

            this.score.stoch.valeur = score

            // this.resultats()
            console.log('Utilisation de Stochs')
            
            this.moyenneMobile()

        })
    }

    public moyenneMobile() {
        let total = 0;
        let diffActuel = 0, diffDer = 0;
        let mm10 = [], mm25 = [];
        let score = 0;
        this._companyService.getMoyenMobile10(this.selectedCompany.symbol).subscribe((data: any) => {
            console.log(data)

            if (data['Technical Analysis: SMA']) {
                for (let [key, value] of Object.entries(data['Technical Analysis: SMA'])) {
                    mm10.push(value)
                    if (mm10.length > 2)
                        break
                }
            }
            else {

                this.openToast("Utilisation de l'API de secours")
                // this.selectedCompany = this.companies[0]
                this._companyService.getSecoursMoyenMobile10().subscribe((data: any) => {
                    console.log(data)

                    for (let [key, value] of Object.entries(data['Technical Analysis: SMA'])) {
                        mm10.push(value)
                        if (mm10.length > 2)
                            break
                    }
                }, (error) => {

                }, () => {
                    if (!!mm25[0] && !!mm10[0]) {

                        diffActuel = parseFloat(mm25[0].SMA) - parseFloat(mm10[0].SMA)
                        diffDer = parseFloat(mm25[1].SMA) - parseFloat(mm10[1].SMA)

                        console.log(mm10)
                        console.log(mm25)

                        if (diffDer < 0) {
                            if ((diffActuel >= -1) && (diffActuel <= 1)) {
                                score = 100;
                            } else {
                                score = 50;
                            }
                        } else if ((diffActuel >= -1) && (diffActuel <= 1)) {
                            score = 0;
                        } else {
                            score = 50;
                        }

                        this.score.mm.valeur = score

                        // this.resultats()
                    }
                })
            }
        }, (error) => {

        }, () => {
            if (!!mm25[0] && !!mm10[0]) {

                diffActuel = parseFloat(mm25[0].SMA) - parseFloat(mm10[0].SMA)
                diffDer = parseFloat(mm25[1].SMA) - parseFloat(mm10[1].SMA)

                console.log(mm10)
                console.log(mm25)

                if (diffDer < 0) {
                    if ((diffActuel >= -1) && (diffActuel <= 1)) {
                        score = 100;
                    } else {
                        score = 50;
                    }
                } else if ((diffActuel >= -1) && (diffActuel <= 1)) {
                    score = 0;
                } else {
                    score = 50;
                }

                this.score.mm.valeur = score

                this.analyseFinanciere()
            }
            console.log('Utilisation de Moyenne mobile')

        })

        this._companyService.getMoyenMobile25(this.selectedCompany.symbol).subscribe((data: any) => {
            if (data['Technical Analysis: SMA']) {
                for (let [key, value] of Object.entries(data['Technical Analysis: SMA'])) {
                    mm25.push(value)
                    if (mm25.length > 2)
                        break
                }
            }
            else {

                this.openToast("Utilisation de l'API de secours")
                // this.selectedCompany = this.companies[0]
                this._companyService.getSecoursMoyenMobile25().subscribe((data: any) => {
                    console.log(data)

                    for (let [key, value] of Object.entries(data['Technical Analysis: SMA'])) {
                        mm25.push(value)
                        if (mm25.length > 2)
                            break
                    }
                }, (error) => {

                }, () => {
                    if (!!mm25[0] && !!mm10[0]) {

                        diffActuel = parseFloat(mm25[0].SMA) - parseFloat(mm10[0].SMA)
                        diffDer = parseFloat(mm25[1].SMA) - parseFloat(mm10[1].SMA)

                        console.log(mm10)
                        console.log(mm25)

                        if (diffDer < 0) {
                            if ((diffActuel >= -1) && (diffActuel <= 1)) {
                                score = 100;
                            } else {
                                score = 50;
                            }
                        } else if ((diffActuel >= -1) && (diffActuel <= 1)) {
                            score = 0;
                        } else {
                            score = 50;
                        }

                        this.score.mm.valeur = score

                        this.resultats()
                    }

                })
            }
        }, (error) => {

        }, () => {
            if (!!mm25[0] && !!mm10[0]) {

                diffActuel = parseFloat(mm25[0].SMA) - parseFloat(mm10[0].SMA)
                diffDer = parseFloat(mm25[1].SMA) - parseFloat(mm10[1].SMA)

                console.log(mm10)
                console.log(mm25)

                if (diffDer < 0) {
                    if ((diffActuel >= -1) && (diffActuel <= 1)) {
                        score = 100;
                    } else {
                        score = 50;
                    }
                } else if ((diffActuel >= -1) && (diffActuel <= 1)) {
                    score = 0;
                } else {
                    score = 50;
                }

                this.score.mm.valeur = score

                this.resultats()
            }
        })
    }

    public analyseFinanciere() {
        let company = this.selectedCompany;
        let moyenne = 0;
        let somme = 0
        //  PER -> 30%
        if (company.PER < 10) {
            somme += 0.3 * 1;
        }
        else {
            if (company.PER < 17) {
                somme += 0.3 * 0.5;

            } else {
                somme += 0;
            }
        }


        // BNPA -> 20%
        if (company.BNPA <= 5) {
            somme += 0.2
        }

        // VBNPA -> 20%
        if (company.VBNPA <= 0) {
            somme += 0.2
        }

        // Rendement -> 30%
        if (company.rendement_18 - company.rendement_17 <= 0) {
            somme += 0.3
        }

        this.score.finance.valeur = somme * 100

        this.resultats()
        // return somme
    }


    public random(min, max) {
        return Math.random() * (max - min) + min;
    }
    public resultats() {
        this.cherche = false;

        this.avis.acheter = false
        this.avis.vendre = false
        this.avis.rien = false

        this.score.random.valeur = this.random(0, 100)

        let totalScore = this.score.var.valeur * this.score.var.ponderation + this.score.bbands.valeur * this.score.bbands.ponderation + this.score.finance.valeur * this.score.finance.ponderation + this.score.mm.valeur * this.score.mm.ponderation + this.score.stoch.valeur * this.score.stoch.ponderation + this.score.random.valeur * this.score.random.ponderation + this.score.res.valeur * this.score.res.ponderation

        console.log(this.score)
        if (totalScore <= 40) {
            this.avis.vendre = true
        }
        else if (totalScore >= 60) {
            this.avis.acheter = true
        }
        else {
            this.avis.rien = true
        }
    }


    public applyFilter(filterValue: string) {
        this.dataSource.filter = filterValue.trim().toLowerCase();

        if (this.dataSource.paginator) {
            this.dataSource.paginator.firstPage();
        }
    }

    getUserInfo(): void {
        this.firstName$ = this._userService.getUser(this.userId).pipe(map(user => user.firstName));
        this.lastName$ = this._userService.getUser(this.userId).pipe(map(user => user.lastName));
        this.pictureUrl$ = this._userService.getUserPictureUrl(this.userId);
    }


    ngOnDestroy() {

    }


}


export interface PeriodicElement {
    name: string;
    position: number;
    weight: number;
    symbol: string;
}
const ELEMENT_DATA: PeriodicElement[] = [
    { position: 1, name: 'Hydrogen', weight: 1.0079, symbol: 'H' },
    { position: 2, name: 'Helium', weight: 4.0026, symbol: 'He' },
    { position: 3, name: 'Lithium', weight: 6.941, symbol: 'Li' },
    { position: 4, name: 'Beryllium', weight: 9.0122, symbol: 'Be' },
    { position: 5, name: 'Boron', weight: 10.811, symbol: 'B' },
    { position: 6, name: 'Carbon', weight: 12.0107, symbol: 'C' },
    { position: 7, name: 'Nitrogen', weight: 14.0067, symbol: 'N' },
    { position: 8, name: 'Oxygen', weight: 15.9994, symbol: 'O' },
    { position: 9, name: 'Fluorine', weight: 18.9984, symbol: 'F' },
    { position: 10, name: 'Neon', weight: 20.1797, symbol: 'Ne' },
    { position: 11, name: 'Sodium', weight: 22.9897, symbol: 'Na' },
    { position: 12, name: 'Magnesium', weight: 24.305, symbol: 'Mg' },
    { position: 13, name: 'Aluminum', weight: 26.9815, symbol: 'Al' },
    { position: 14, name: 'Silicon', weight: 28.0855, symbol: 'Si' },
    { position: 15, name: 'Phosphorus', weight: 30.9738, symbol: 'P' },
    { position: 16, name: 'Sulfur', weight: 32.065, symbol: 'S' },
    { position: 17, name: 'Chlorine', weight: 35.453, symbol: 'Cl' },
    { position: 18, name: 'Argon', weight: 39.948, symbol: 'Ar' },
    { position: 19, name: 'Potassium', weight: 39.0983, symbol: 'K' },
    { position: 20, name: 'Calcium', weight: 40.078, symbol: 'Ca' },
];


@Component({
    selector: 'dialog-overview-example-dialog',
    templateUrl: 'dialog-overview-example-dialog.html',
  })
  export class DialogOverviewExampleDialog {
  
    constructor(
      public dialogRef: MatDialogRef<DialogOverviewExampleDialog>,
      ) {}
  
    onNoClick(): void {
      this.dialogRef.close();
    }
  
  }