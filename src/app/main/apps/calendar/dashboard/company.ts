export class Company {
  uid: string;
  email: string;
  firstName: string;
  lastName: string;
  birthday: string;
  company: string;
  job: string;
  phone: string;
  isManager: boolean;
  picturePath: string;
  pictureUrl: string; 
}
