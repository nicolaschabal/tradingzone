import { NewsService } from './news.service';
import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import {
    MatTableModule,MatButtonModule, MatDatepickerModule, MatDialogModule, MatFormFieldModule, MatIconModule, MatInputModule, MatSlideToggleModule, MatToolbarModule, MatTooltipModule, MatProgressSpinnerModule
} from '@angular/material';
import { MatPaginatorModule } from '@angular/material';
import { ColorPickerModule } from 'ngx-color-picker';
import { CalendarModule as AngularCalendarModule, DateAdapter } from 'angular-calendar';
import { adapterFactory } from 'angular-calendar/date-adapters/date-fns';
import { TranslateModule } from '@ngx-translate/core';
import {MatListModule} from '@angular/material/list';
import { FuseSharedModule } from '@fuse/shared.module';
import { FuseConfirmDialogModule } from '@fuse/components';

import { CompanyService } from 'app/main/apps/calendar/company.service';
import { CalendarService } from 'app/main/apps/calendar/calendar.service';
import { AuthGuard } from 'app/main/guards/auth.guard';
import { DashboardComponent } from "./dashboard/dashboard.component";
import localeFr from '@angular/common/locales/fr';
import { registerLocaleData } from '@angular/common';
import { ChatPanelModule } from 'app/layout/components/chat-panel/chat-panel.module';
import {MatGridListModule} from '@angular/material/grid-list';
import {MatSnackBarModule} from '@angular/material/snack-bar';

import {MatAutocompleteModule} from '@angular/material/autocomplete';
import {MatCardModule} from '@angular/material/card';
import {ScrollDispatchModule} from '@angular/cdk/scrolling';
import {MatTabsModule} from '@angular/material/tabs';




registerLocaleData(localeFr);
const routes: Routes = [
    {
        path     : 'dashboard',
        component: DashboardComponent, 
        children : [],
        canActivate: [ AuthGuard ]
    }
];

@NgModule({
    declarations   : [
        DashboardComponent,
    ],
    imports        : [
        RouterModule.forChild(routes),
        MatAutocompleteModule,
        MatTableModule,
        MatPaginatorModule,
        MatListModule,
        ChatPanelModule,
        MatTabsModule,
        MatButtonModule,
        MatDatepickerModule,
        MatDialogModule,
        MatFormFieldModule,
        MatIconModule,
        MatInputModule,
        MatSlideToggleModule,
        MatToolbarModule,
        MatTooltipModule,
        MatCardModule,
        MatProgressSpinnerModule,
        ScrollDispatchModule,
        MatDialogModule,
        TranslateModule,
        MatGridListModule,
        MatSnackBarModule,

        AngularCalendarModule.forRoot({
            provide   : DateAdapter,
            useFactory: adapterFactory
        }),
        ColorPickerModule,

        FuseSharedModule,
        FuseConfirmDialogModule
    ],
    providers      : [
        CalendarService, CompanyService, NewsService
    ],
    entryComponents: [
    ]
})
export class CalendarModule
{
}
