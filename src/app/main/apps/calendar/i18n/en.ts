export const locale = {
    lang: 'en',
    data: {
        'CALENDAR': {
            'WELCOME'                 : 'Welcome',
            'LANG'                    : 'en',
            'CALENDAR'                : 'Calendar',
            'TITLE'                   : 'Title',
            'PRIMARY_COLOR'           : 'Primary color',
            'SECONDARY_COLOR'         : 'Secondary color',
            'ALL_DAY'                 : 'All day',
            'START_DATE'              : 'Start date',
            'START_TIME'              : 'Start time',
            'END_DATE'                : 'End date',
            'END_TIME'                : 'End time',
            'LOCATION'                : 'Location',
            'NOTES'                   : 'Notes',
            'DELETE'                  : 'Delete',
            'ADD'                     : 'Add',
            'SAVE'                    : 'Save',
            'DIALOG_TITLE'            : 'New event',
            'DAY'                     : 'Day',
            'SEARCH'                  : 'Search',
            'TODAY'                   : 'Today',
            'WEEK'                    : 'Week',
            'MONTH'                   : 'Month',
            'DELETE_CONFIRM'          : 'Are you sure you want to delete?',
            'CONFIRM_TITLE'           : 'Confirm',
            'CONFIRM'                 : 'Confirm',
            'CANCEL'                  : 'Cancel'
        }
    }
};
