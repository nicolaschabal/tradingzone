export const locale = {
    lang: 'fr',
    data: {
        'CALENDAR': {
            'WELCOME'                 : 'Bienvenue',
            'LANG'                    : 'fr',
            'CALENDAR'                : 'Calendrier',
            'TITLE'                   : 'Titre',
            'PRIMARY_COLOR'           : 'Couleur primaire',
            'SECONDARY_COLOR'         : 'Couleur secondaire',
            'ALL_DAY'                 : 'Toute la journée',
            'START_DATE'              : 'Date de début',
            'START_TIME'              : 'Heure de début',
            'END_DATE'                : 'Date de fin',
            'END_TIME'                : 'Heure de fin',
            'LOCATION'                : 'Lieu',
            'NOTES'                   : 'Commentaires',
            'DELETE'                  : 'Supprimer',
            'ADD'                     : 'Ajouter',
            'SAVE'                    : 'Sauvegarder',
            'DIALOG_TITLE'            : 'Nouvel événement',
            'DAY'                     : 'Jour',
            'SEARCH'                  : 'Recherche',
            'TODAY'                   : 'Aujourd’hui',
            'WEEK'                    : 'Semaine',
            'MONTH'                   : 'Mois',
            'DELETE_CONFIRM'          : "Êtes-vous sûrs de vouloir supprimer l'événement ?",
            'CONFIRM_TITLE'           : 'Confirmation',
            'CONFIRM'                 : 'Valider',
            'CANCEL'                  : 'Annuler'
        }
    }
};