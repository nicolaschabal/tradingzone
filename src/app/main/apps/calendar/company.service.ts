import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import * as Highcharts from 'highcharts';
@Injectable({
  providedIn: 'root'
})
export class CompanyService {
  apiKey = '2TQL1CQBZFZ05VQG';
  charts = [];



  constructor(private http: HttpClient) { }


  getTimeSeriesDaily(symbol: String) {
    let url = "https://www.alphavantage.co/query?function=TIME_SERIES_DAILY&symbol=" + symbol + "&outputsize=full&apikey=R2V6EV7LBQ02WEPK";
    return this.http.get(url)
  }
  getSecoursTimeSeriesDaily(symbol: String) {
    let url = "https://www.alphavantage.co/query?function=TIME_SERIES_DAILY&symbol=MSFT&outputsize=full&apikey=demo";
    return this.http.get(url)
  }



  getGlobalQuote(symbol: String) {
    // let url = "https://www.alphavantage.co/query?function=GLOBAL_QUOTE&symbol=MSFT&apikey=demo"
    let url = "https://www.alphavantage.co/query?function=GLOBAL_QUOTE&symbol=" + symbol + "&apikey=EK2O9BFSXJYLO762"
    return this.http.get(url);
  }
  getSecoursGlobalQuote(symbol: String) {
    let url = "https://www.alphavantage.co/query?function=GLOBAL_QUOTE&symbol=MSFT&apikey=demo"
    return this.http.get(url);
  }



  getBbands(symbol){
    let url = "https://www.alphavantage.co/query?function=BBANDS&symbol="+symbol+"&interval=1min&time_period=5&series_type=close&nbdevup=3&nbdevdn=3&apikey=T9KNO3FGP117IXIE"
    return this.http.get(url);
  }
  getSecoursBbands(symbol){
    let url = "https://www.alphavantage.co/query?function=BBANDS&symbol=MSFT&interval=weekly&time_period=5&series_type=close&nbdevup=3&nbdevdn=3&apikey=demo"
    return this.http.get(url);
  }

  getDatas(symbol){
    symbol = symbol.split(".")[0]
    let url = "https://api.stockdio.com/data/financial/prices/v1/GetHistoricalPrices?app-key=66C2D970289744F09D4561C1B5D368D1&stockExchange=PEX&symbol="+symbol+"&from=2018-11-29&to=2019-11-29"
    return this.http.get(url);
  }
  getLatestPrice(symbol){
    symbol = symbol.split(".")[0]
    let url = "https://api.stockdio.com/data/financial/prices/v1/getlatestprices?app-key=66C2D970289744F09D4561C1B5D368D1&stockExchange=PEX&symbols="+symbol
    return this.http.get(url);
  }

  
  getStochs(symbol){
    let url = "https://www.alphavantage.co/query?function=STOCH&symbol="+symbol+"&interval=1min&apikey=8J3C8K8DXMB5HXK2"
    return this.http.get(url);
  }
  getSecoursStochs(){
    let url = "https://www.alphavantage.co/query?function=STOCH&symbol=MSFT&interval=daily&apikey=demo"
    return this.http.get(url);
  }



  getMoyenMobile10(symbol){
    let url = "https://www.alphavantage.co/query?function=SMA&symbol="+symbol+"&interval=1min&time_period=10&series_type=open&apikey=OF7BJ2RKFGVDPCSE"
    return this.http.get(url);
  }
  getMoyenMobile25(symbol){
    let url = "https://www.alphavantage.co/query?function=SMA&symbol="+symbol+"&interval=1min&time_period=25&series_type=open&apikey=FQPQ5O1FF7K9152Q"
    return this.http.get(url);
  }
  getSecoursMoyenMobile10(){
    let url = "https://www.alphavantage.co/query?function=SMA&symbol=MSFT&interval=weekly&time_period=10&series_type=open&apikey=demo"
    return this.http.get(url);
  }
  getSecoursMoyenMobile25(){
    let url = "https://www.alphavantage.co/query?function=SMA&symbol=MSFT&interval=weekly&time_period=10&series_type=open&apikey=demo"
    return this.http.get(url);
  }




  createChart(container, symbol, data = null) {
    let e = document.createElement("div");

    container.appendChild(e);

    let options: any = this.transformConfiguration(symbol, data);

    if (options.chart != null) {
      options.chart['renderTo'] = e;
    }
    else {
      options.chart = {
        'renderTo': e
      };
    }

    this.charts.push(new Highcharts.Chart(options));
  }

  getCharts() {
    return this.charts;
  }
  
  chartIntraDay(symbol, data) {
    var config = {
      chart: { type: 'spline' },
      title: { text: symbol },
      xAxis: {
        type: 'datetime'
      },
      series: [{
        name: symbol,
        data: data
      }],
      rangeSelector: {
        buttons: [{
          type: 'hour',
          count: 1,
          text: '1h'
        }, {
          type: 'day',
          count: 1,
          text: '1D'
        }, {
          type: 'all',
          count: 1,
          text: 'All'
        }],
        selected: 1,
        inputEnabled: false
      }
    };

    return config;
  };

  transformConfiguration(symbol, data) {
    let chartConfig = this.chartIntraDay(symbol, data);

    return chartConfig;
  }

  createStockQuery(tickerSymbol) {
    var url = 'https://www.alphavantage.co/query?function=TIME_SERIES_INTRADAY&symbol=' + tickerSymbol + '&interval=5min&apikey=' + this.apiKey;

    return encodeURI(url);
  };

  loadData(symbol, callback) {
    this.http.get(this.createStockQuery(symbol)).subscribe(this.onDataReceived.bind(this, symbol, callback));
  };

  onDataReceived(symbol, callback, rawData) {
    var highchartsData = this.transformDataForHighCharts(rawData);

    callback(symbol, highchartsData);

  };

  transformDataForHighCharts(rawData) {
    var quotes = rawData['Time Series (5min)'],data = [], i, item;

    for (var each in quotes) {
      item = quotes[each];

      data.push([new Date(each).getTime(),
      parseFloat(item["4. close"])]);
    }

    return data;
  };


}
