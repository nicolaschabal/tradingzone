export const locale = {
    lang: 'fr',
    data: {
        'CONTACTS': {
            'NAME'                      :'Prénom',
            'LAST_NAME'                 :'Nom',
            'NICK_NAME'                 :'Pseudo',
            'PHONE_NUMBER'              :'Téléphone',
            'EMAIL'                     :'Email',
            'COMPANY'                   :'Compagnie',
            'JOB_TITLE'                 :'Profession',
            'BIRTHDAY'                  :'Anniversaire',
            'ADDRESS'                   :'Adresse',
            'NOTES'                     :'Notes',
            'DELETE'                    :'SUPPRIMER',
            'ADD'                       :'AJOUTER',
            'SAVE'                      :'SAUVEGARDER',
            'ALL_CONTACTS'              :'Tout les contacts',
            'FREEQUENTLY_CONTACTED'     :'Contacts fréquent',
            'STARRED_CONTACTS'          :'Favoris',
            'SEARCH_FOR_A_CONTACT'      :'Rechercher un contact',
            'NEW_CONTACT'               :'Nouveau contact',
            'REMOVE'                    :'SUPPRIMER',
        }
    }
};
