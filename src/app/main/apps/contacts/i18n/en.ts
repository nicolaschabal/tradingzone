export const locale = {
    lang: 'en',
    data: {
        'CONTACTS': {
            'NAME'                      :'Name',
            'LAST_NAME'                 :'Last Name',
            'NICK_NAME'                 :'Nick Name',
            'PHONE_NUMBER'              :'Phone Number',
            'EMAIL'                     :'Email',
            'COMPANY'                   :'Company',
            'JOB_TITLE'                 :'Job title',
            'BIRTHDAY'                  :'Birthday',
            'ADDRESS'                   :'Address',
            'NOTES'                     :'Notes',
            'DELETE'                    :'DELETE',
            'ADD'                       :'ADD',
            'SAVE'                      :'SAVE',
            'ALL_CONTACTS'              :'All Contacts',
            'FREEQUENTLY_CONTACTED'     :'Freequently contacted',
            'STARRED_CONTACTS'          :'Starred contacts',
            'SEARCH_FOR_A_CONTACT'      :'Search for a contact',
            'NEW_CONTACT'               :'New contact',
            'REMOVE'                    :'Remove',
        }
    }
};
