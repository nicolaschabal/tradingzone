import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { MatButtonModule, MatFormFieldModule, MatIconModule, MatInputModule, MatProgressSpinnerModule } from '@angular/material';

import { FuseSharedModule } from '@fuse/shared.module';
import { AuthGuard } from 'app/main/guards/auth.guard';

import { IconsComponent } from 'app/main/ui/icons/icons.component';

const routes: Routes = [
    {
        path     : 'icons',
        component: IconsComponent,
        canActivate: [ AuthGuard ]
    }
];

@NgModule({
    declarations: [
        IconsComponent
    ],
    imports     : [
        RouterModule.forChild(routes),

        MatButtonModule,
        MatFormFieldModule,
        MatIconModule,
        MatInputModule,
        MatProgressSpinnerModule,

        FuseSharedModule
    ]
})
export class UIIconsModule
{
}
