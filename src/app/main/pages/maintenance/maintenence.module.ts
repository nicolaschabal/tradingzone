 import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';

import { FuseSharedModule } from '@fuse/shared.module';

import { MaintenanceComponent } from 'app/main/pages/maintenance/maintenance.component';
import { AuthGuard } from '../../guards/auth.guard';
import { SecureInnerPagesGuard } from "../../guards/secure-inner-pages.guard";

const routes = [
    {
        path     : 'maintenance',
        component: MaintenanceComponent,
        canActivate:  [ SecureInnerPagesGuard ]
    }
    
];

@NgModule({
    declarations: [
        MaintenanceComponent
    ],
    imports     : [
        RouterModule.forChild(routes),

        FuseSharedModule
    ]
})
export class  MaintenanceModule
{
}
