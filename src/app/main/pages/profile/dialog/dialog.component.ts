import { UserService } from 'app/main/apps/users/user.service';
import { Component, OnInit } from '@angular/core';
import { AngularFireStorage, AngularFireUploadTask } from '@angular/fire/storage';
import { AngularFirestore } from '@angular/fire/firestore';
import { Observable } from 'rxjs';
import { tap, finalize } from 'rxjs/operators';

@Component({
  selector: 'app-dialog',
  templateUrl: './dialog.component.html',
  styleUrls: ['./dialog.component.scss']
})
export class DialogComponent implements OnInit {
  ngOnInit(): void { }

  // Main task 
  task: AngularFireUploadTask;

  // Progress monitoring
  percentage: Observable<number>;

  snapshot: Observable<any>;

  // Download URL
  downloadURL: Observable<string>;

  // State for dropzone CSS toggling
  isHovering: boolean;

  constructor(private storage: AngularFireStorage, private db: AngularFirestore, private userService: UserService) { }


  toggleHover(event: boolean) {
    this.isHovering = event;
  }


  startUpload(event: FileList) {

    let currentUser = this.userService.getUserFromLocalStorage();

    console.log(currentUser.uid);

    // The File object
    const file = event.item(0)

    // Si il y a une photo déjà enregistré dans le storage on la supprime
    if (currentUser.picturePath !== '' && file.type.split('/')[0] === 'image') {
      this.storage.ref(currentUser.picturePath).delete();
    }


    // Client-side validation example
    if (file.type.split('/')[0] !== 'image') {
      console.error('unsupported file type :( ')
      return;
    }

    // The storage path
    const path = `user_picture/${new Date().getTime()}_${file.name}`;


    // The main task
    this.task = this.storage.upload(path, file);

    // Progress monitoring
    this.percentage = this.task.percentageChanges()
    this.snapshot = this.task.snapshotChanges();
    console.log(this.snapshot);
    /*this.snapshot = this.task.snapshotChanges().pipe(
      tap(snap => {
        if (snap.bytesTransferred === snap.totalBytes) {
          // Update firestore on completion
          this.storage.ref(path).getDownloadURL().subscribe((res) => {
            this.storage.ref(path)
            this.db.doc(`users/${currentUser.uid}`).update({ picturePath: path, pictureUrl: res });
            
          }) 
        }
      })
    )*/
    const ref = this.storage.ref(path);
    this.snapshot   = this.task.snapshotChanges().pipe(
      tap(console.log),
      // The file's download URL
      finalize( async() =>  {
        this.downloadURL = await ref.getDownloadURL().toPromise();
        this.db.doc(`users/${currentUser.uid}`).update({ picturePath: path, pictureUrl: this.downloadURL });
        //this.db.collection('files').add( { downloadURL: this.downloadURL, path });
        this.userService.setCurrentUser(currentUser.uid);
      }),
    );
  }

  // Determines if the upload task is active
  isActive(snapshot) {
    return snapshot.state === 'running' && snapshot.bytesTransferred < snapshot.totalBytes
  }
}
