import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { MatButtonModule, MatDividerModule, MatIconModule, MatTabsModule, MatFormFieldModule, MatInputModule } from '@angular/material';

import { FuseSharedModule } from '@fuse/shared.module';
import {MatListModule} from '@angular/material/list'; 
import { ProfileService } from 'app/main/pages/profile/profile.service';
import { ProfileComponent } from 'app/main/pages/profile/profile.component';
import { ProfileTimelineComponent } from 'app/main/pages/profile/tabs/timeline/timeline.component';
import { ProfileAboutComponent } from 'app/main/pages/profile/tabs/about/about.component';
import { ProfilePhotosVideosComponent } from 'app/main/pages/profile/tabs/photos-videos/photos-videos.component';
import { AuthGuard } from '../../guards/auth.guard';
import { DialogComponent } from './dialog/dialog.component';
import {MatDialogModule} from '@angular/material/dialog';
import { DropzoneDirective } from './drop-zone.directive';
import {MatProgressSpinnerModule} from '@angular/material/progress-spinner';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';

import { AngularFireStorage, AngularFireStorageModule } from '@angular/fire/storage';


const routes = [
    {
        path     : 'profile',
        component: ProfileComponent,
        resolve  : {
            profile: ProfileService
        },
        canActivate:  [ AuthGuard ]
    }
];

@NgModule({
    declarations: [
        ProfileComponent,
        ProfileTimelineComponent,
        ProfileAboutComponent,
        ProfilePhotosVideosComponent,
        DialogComponent,
        DropzoneDirective
    ],
    imports     : [
        RouterModule.forChild(routes),
        MatListModule,
        MatProgressSpinnerModule,
        MatInputModule,
        MatDialogModule,
        MatButtonModule,
        MatDividerModule,
        MatIconModule,
        MatTabsModule, 
        AngularFireStorageModule,  
        MatFormFieldModule,

        FuseSharedModule
    ],
    providers   : [
        ProfileService, AngularFireStorage
    ],
    entryComponents: [DialogComponent]
})
export class  ProfileModule
{
}
