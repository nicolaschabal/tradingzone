import { UserService } from './../../../../apps/users/user.service';
import { Component, OnDestroy, OnInit, ViewEncapsulation, Pipe } from '@angular/core';
import { Subject, combineLatest } from 'rxjs';
import { fuseAnimations } from '@fuse/animations';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { User } from 'app/main/apps/users/user';
import { _throw } from 'rxjs/observable/throw';

@Component({
    selector: 'profile-about',
    templateUrl: './about.component.html',
    styleUrls: ['./about.component.scss'],
    encapsulation: ViewEncapsulation.None,
    animations: fuseAnimations
})
export class ProfileAboutComponent implements OnInit, OnDestroy {
    about: any;
    timeline: any;
    profilForm: FormGroup;
    authError: any;
    user: User;
    userId: string;
    isDifferent: boolean;

    // Private
    private _unsubscribeAll: Subject<any>;

    constructor(
        private _formBuilder: FormBuilder,
        private _userService: UserService
    ) {
        // Set the private defaults
        this._unsubscribeAll = new Subject();
    }

    ngOnInit(): void {
        this.userId = this._userService.getUserFromLocalStorage().uid;

        // Pré remplir les champs avec la fonction lightUser du service qui renvoie le user en base avec les champs nécéssaires
        this._userService.getLightUser(this.userId).subscribe(lightUser => {
            this.profilForm.setValue(lightUser);
        });

        // Angular reactive form, permet la validation des forms
        this.profilForm = this._formBuilder.group({
            firstName:  ['', [Validators.required]],
            lastName:   ['', [Validators.required]],
            birthday:   ['', [Validators.required]],
            phone:      ['', [Validators.required]],
            company:    ['', [Validators.required]],
            job:        ['', [Validators.required]],
        });

        combineLatest(this.profilForm.valueChanges, this._userService.getLightUser(this.userId)).subscribe(
            ([form, db]) => {
                JSON.stringify(form) === JSON.stringify(db) ? this.isDifferent = false : this.isDifferent = true;
                console.log(this.isDifferent);
            }
        );
        console.log(this.isDifferent);
    }

    onClickModifyProfile() {

    }

    onClickDone() {
        
    }

    onClickUpdate() {
        console.log(this.profilForm.value);
        this._userService.updateUser(this.profilForm.value);
    }


    ngOnDestroy(): void {
        // Unsubscribe from all subscriptions
        this._unsubscribeAll.next();
        this._unsubscribeAll.complete();
    }
}
