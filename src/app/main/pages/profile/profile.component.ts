import { Observable, Subject, combineLatest } from 'rxjs';
import { Component, ViewEncapsulation, OnInit } from '@angular/core';

import { fuseAnimations } from '@fuse/animations';
import { MatDialog } from '@angular/material';
import { DialogComponent } from './dialog/dialog.component';
import { UserService } from 'app/main/apps/users/user.service';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { map } from 'rxjs/operators';

@Component({
    selector: 'profile',
    templateUrl: './profile.component.html',
    styleUrls: ['./profile.component.scss'],
    encapsulation: ViewEncapsulation.None,
    animations: fuseAnimations
})


export class ProfileComponent implements OnInit {
    userId: string
    firstName$: Observable<string>;
    lastName$: Observable<string>;
    pictureUrl$: Observable<string>;

    about: any;
    timeline: any;
    profilForm: FormGroup;
    authError: any;
    user: any = '';
    
    isDifferent: boolean;
    isModify: boolean = false;
    isCancel: boolean = false;

    // Private
    private _unsubscribeAll: Subject<any>;

    constructor(
        public dialog: MatDialog,
        private _userService: UserService,
        private _formBuilder: FormBuilder
    ) {
        // Set the private defaults
        this._unsubscribeAll = new Subject();
    }

    ngOnInit(): void {
        this.userId = this._userService.getUserFromLocalStorage().uid;
        this.getUserInfo();
        this._userService.getUser(this.userId).subscribe(u => this.user = u);

        // Pré remplir les champs avec la fonction lightUser du service qui renvoie le user en base avec les champs nécéssaires
        this._userService.getLightUser(this.userId).subscribe(lightUser => {
            this.profilForm.setValue(lightUser);
        });

        // Angular reactive form, permet la validation des forms
        this.profilForm = this._formBuilder.group({
            firstName: ['', [Validators.required]],
            lastName: ['', [Validators.required]],
            birthday: ['', [Validators.required]],
            phone: ['', [Validators.required]],
            company: ['', [Validators.required]],
            job: ['', [Validators.required]],
        });

        combineLatest(this.profilForm.valueChanges, this._userService.getLightUser(this.userId)).subscribe(
            ([form, db]) => {
                JSON.stringify(form) === JSON.stringify(db) ? this.isDifferent = false : this.isDifferent = true;
                console.log(this.isDifferent);
            }
        );
    }

    onClickModifyProfile() {
        this.isModify = true;
        this.isCancel = false;
    }

    onClickCancel() {
        this.isCancel = true;
        this.isModify = false;
    }

    onClickUpdate() {
        console.log(this.profilForm.value);
        this._userService.updateUser(this.profilForm.value);
        this.isModify = false;
    }
    
    getUserInfo(): void {
        this.firstName$ = this._userService.getUser(this.userId).pipe(map(user => user.firstName));
        this.lastName$ = this._userService.getUser(this.userId).pipe(map(user => user.lastName));
        this.pictureUrl$ = this._userService.getUserPictureUrl(this.userId);
    }
    
    openDialog() {
        const dialogRef = this.dialog.open(DialogComponent);
    } 

    ngOnDestroy(): void {
        // Unsubscribe from all subscriptions
        this._unsubscribeAll.next();
        this._unsubscribeAll.complete();
    }
}
