import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { MatButtonModule, MatCheckboxModule, MatFormFieldModule, MatIconModule, MatInputModule } from '@angular/material';
import {MatProgressSpinnerModule} from '@angular/material/progress-spinner';
import { FuseSharedModule } from '@fuse/shared.module';
import { Login2Component } from 'app/main/pages/authentication/login-2/login-2.component';
import { SecureInnerPagesGuard } from '../../../guards/secure-inner-pages.guard';
import { AuthentificationService } from '../authentification.service';

const routes = [
    {
        path     : 'auth/login-2',
        component: Login2Component,
        canActivate: [SecureInnerPagesGuard],
    }
];

@NgModule({
    declarations: [
        Login2Component
    ],
    imports     : [
        RouterModule.forChild(routes),

        MatProgressSpinnerModule,
        MatButtonModule,
        MatCheckboxModule,
        MatFormFieldModule,
        MatIconModule,
        MatInputModule,

        FuseSharedModule
    ],
    providers:[SecureInnerPagesGuard, AuthentificationService]
})
export class  Login2Module
{
}
