import { Observable } from 'rxjs';
import { Component, OnInit, ViewEncapsulation } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { FuseConfigService } from '@fuse/services/config.service';
import { fuseAnimations } from '@fuse/animations';
import { AuthentificationService } from "../authentification.service";
import { FuseProgressBarService } from '@fuse/components/progress-bar/progress-bar.service';

@Component({
	selector: 'login-2',
	templateUrl: './login-2.component.html',
	styleUrls: ['./login-2.component.scss'],
	encapsulation: ViewEncapsulation.None,
	animations: fuseAnimations
})
export class Login2Component implements OnInit {
	loginForm: FormGroup;
	email: string;
	password: string;
	authError: any;
	loading$: Observable<boolean>;
	loadingData$: Observable<boolean>;
	/**
	 * Constructor
	 *
	 * @param {FuseConfigService} _fuseConfigService
	 * @param {FormBuilder} _formBuilder
	 */
	constructor(
		public authService: AuthentificationService,
		private _navigationService: FuseProgressBarService,
		private _fuseConfigService: FuseConfigService,
		private _formBuilder: FormBuilder,
	) {

		// Configure the layout
		this._fuseConfigService.config = {
			layout: {
				navbar: {
					hidden: true
				},
				toolbar: {
					hidden: true
				},
				footer: {
					hidden: true
				},
				sidepanel: {
					hidden: true
				}
			}
		};
	}

	// -----------------------------------------------------------------------------------------------------
	// @ Lifecycle hooks
	// -----------------------------------------------------------------------------------------------------

	/**
	 * On init
	 */
	ngOnInit(): void {
		this.loginForm = this._formBuilder.group({
			email: ['', [Validators.required, Validators.email]],
			password: ['', Validators.required]
		});
	}

	get emailForm() {
		return this.loginForm.get('email');
	}

	get passwordForm() {
		return this.loginForm.get('password');
	}

	/**
	 * @description Fait appel au service pour l'authentification et si il y a une erreur ouvre une snackbar
	 */
	async logIn() {
		this._navigationService.navigationState();
		this.loadingData$ = this._navigationService.loadingData$;
		this.loading$ = this.authService.isLoading$;
		await this.authService.login(this.emailForm.value, this.passwordForm.value);
		this.authService.authError$.subscribe((error) => {
			if (!!error) {
				this.authService.openErrorMessage(error.code, 'FERMER');
			}
		});
	}
}
