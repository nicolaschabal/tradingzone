import { async } from '@angular/core/testing';
import { Component, ViewEncapsulation } from '@angular/core';

import { FuseConfigService } from '@fuse/services/config.service';
import { fuseAnimations } from '@fuse/animations';
import { AuthentificationService } from '../authentification.service';
import { AngularFireAuth } from "@angular/fire/auth";

@Component({
    selector     : 'mail-confirm',
    templateUrl  : './mail-confirm.component.html',
    styleUrls    : ['./mail-confirm.component.scss'],
    encapsulation: ViewEncapsulation.None,
    animations   : fuseAnimations
})
export class MailConfirmComponent
{
    public currentUserEmail: string;

    /**
     * Constructor
     *
     * @param {FuseConfigService} _fuseConfigService
     * @param {AuthentificationService} _authService
     * @param {AngularFireAuth} _afAuth
     */
    constructor(
        private _fuseConfigService: FuseConfigService,
        private _authService: AuthentificationService,
        private _afAuth: AngularFireAuth 
    )
    {
        this.getUserEmail()
        // Configure the layout
        this._fuseConfigService.config = {
            layout: {
                navbar   : {
                    hidden: true
                },
                toolbar  : {
                    hidden: true
                },
                footer   : {
                    hidden: true
                },
                sidepanel: {
                    hidden: true
                }
            }
        };
    }

    async getUserEmail() {
        const user: firebase.User = await this._authService.getUser();
        this.currentUserEmail = user.email;
    }

    public senVerificationEmail() {
       this._authService.sendVerificationMail();
    }
}
