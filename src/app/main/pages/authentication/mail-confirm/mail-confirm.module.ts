import { NgModule } from '@angular/core';
import { RouterModule, CanActivate } from '@angular/router';
import { MatIconModule } from '@angular/material';

import { FuseSharedModule } from '@fuse/shared.module';

import { MailConfirmComponent } from 'app/main/pages/authentication/mail-confirm/mail-confirm.component';
import { SecureInnerPagesGuard } from '../../../guards/secure-inner-pages.guard';

const routes = [
    {
        path     : 'auth/mail-confirm',
        component: MailConfirmComponent,
        canActivate: [SecureInnerPagesGuard]
    }
];

@NgModule({
    declarations: [
        MailConfirmComponent
    ],
    imports     : [
        RouterModule.forChild(routes),

        MatIconModule,

        FuseSharedModule
    ]
})
export class  MailConfirmModule
{
}
