import { NgModule } from '@angular/core'; 
import { RouterModule } from '@angular/router';
import { MatButtonModule, MatCheckboxModule, MatFormFieldModule, MatIconModule, MatInputModule } from '@angular/material';
import { FuseSharedModule } from '@fuse/shared.module';

import { LoginComponent } from 'app/main/pages/authentication/login/login.component';
import { SecureInnerPagesGuard } from "../../../guards/secure-inner-pages.guard";

const routes = [
    {
        path     : 'auth/login',
        component: LoginComponent,
        canActivate: [SecureInnerPagesGuard]
        
    }
];

@NgModule({
    declarations: [
        LoginComponent,
    ],
    imports     : [
        RouterModule.forChild(routes),

        MatButtonModule,
        MatCheckboxModule,
        MatFormFieldModule,
        MatIconModule,
        MatInputModule,

        FuseSharedModule
    ],
    providers: [SecureInnerPagesGuard],
})
export class LoginModule
{
}
