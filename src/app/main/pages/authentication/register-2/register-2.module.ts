import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { MatButtonModule, MatCheckboxModule, MatFormFieldModule, MatIconModule, MatInputModule } from '@angular/material';
import { FuseSharedModule } from '@fuse/shared.module';
import {MatProgressSpinnerModule} from '@angular/material/progress-spinner';
import { Register2Component } from 'app/main/pages/authentication/register-2/register-2.component';
import { SecureInnerPagesGuard } from '../../../guards/secure-inner-pages.guard';
import {MatStepperModule} from '@angular/material/stepper';

const routes = [
    {
        path     : 'auth/register-2',
        component: Register2Component,
        canActivate: [SecureInnerPagesGuard]
    }
];

@NgModule({
    declarations: [
        Register2Component
    ],
    imports     : [
        RouterModule.forChild(routes),
        MatProgressSpinnerModule,
        MatButtonModule,
        MatCheckboxModule,
        MatFormFieldModule,
        MatIconModule,
        MatInputModule,
        MatStepperModule,

        FuseSharedModule
    ]
})
export class  Register2Module
{
}
