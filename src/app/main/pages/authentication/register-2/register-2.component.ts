
import { Component, OnDestroy, OnInit, ViewEncapsulation } from '@angular/core';
import { AbstractControl, FormBuilder, FormGroup, ValidationErrors, ValidatorFn, Validators } from '@angular/forms';
import { Subject, Observable } from 'rxjs';
import { takeUntil } from 'rxjs/operators';
import { AuthentificationService } from "../authentification.service";
import { FuseConfigService } from '@fuse/services/config.service';
import { fuseAnimations } from '@fuse/animations';
import { FuseProgressBarService } from "@fuse/components/progress-bar/progress-bar.service";

@Component({
    selector: 'register-2',
    templateUrl: './register-2.component.html',
    styleUrls: ['./register-2.component.scss'],
    encapsulation: ViewEncapsulation.None,
    animations: fuseAnimations
})
export class Register2Component implements OnInit, OnDestroy {
    registerForm: FormGroup;
    authError: any;
    loading$: Observable<boolean>;
    loadingData$: Observable<boolean>;

    formGroup: FormGroup;
    nameFormGroup: FormGroup;
    emailFormGroup: FormGroup;


    // Private
    private _unsubscribeAll: Subject<any>;

    constructor(
        public authService: AuthentificationService,
        private _navigationService: FuseProgressBarService,
        private _fuseConfigService: FuseConfigService,
        private _formBuilder: FormBuilder
    ) {
        // Configure the layout
        this._fuseConfigService.config = {
            layout: {
                navbar: {
                    hidden: true
                },
                toolbar: {
                    hidden: true
                },
                footer: {
                    hidden: true
                },
                sidepanel: {
                    hidden: true
                }
            }
        };

        // Set the private defaults
        this._unsubscribeAll = new Subject();
    }

    // -----------------------------------------------------------------------------------------------------
    // @ Lifecycle hooks
    // -----------------------------------------------------------------------------------------------------

    /**
     * On init
     */
    ngOnInit(): void {
        this.registerForm = this._formBuilder.group({
            firstName: ['', [Validators.required]],
            lastName: ['', [Validators.required]],
            email: ['', [Validators.required, Validators.email]],
            isManager: [false],
            picturePath: [''],
            pictureUrl: [''],
            password: ['', Validators.required],
            passwordConfirm: ['', [Validators.required, confirmPasswordValidator]]

        });


        this.formGroup = this._formBuilder.group({
            formArray: this._formBuilder.array([
                this._formBuilder.group({
                    firstName: ['', Validators.required],
                    lastName: ['', Validators.required],
                    email: ['', [Validators.required, Validators.email]],
                    birthday: ['N/A']
                }),
                this._formBuilder.group({
                    company: ['', Validators.required],
                    job: ['', Validators.required],
                    phone: ['', Validators.required]
                }),
                this._formBuilder.group({
                    password: ['', Validators.required],
                    passwordConfirm: ['', [Validators.required, confirmPasswordValidator]],
                    isManager: [false],
                    picturePath: [''],
                    pictureUrl: ['']
                })
            ]),
        });



        // Update the validity of the 'passwordConfirm' field
        // when the 'password' field changes
        this.formGroup.get('password').valueChanges
            .pipe(takeUntil(this._unsubscribeAll))
            .subscribe(() => {
                this.formGroup.get('passwordConfirm').updateValueAndValidity();
            });
    }

    /** Returns a FormArray with the name 'formArray'. */
    get formArray(): AbstractControl | null { return this.formGroup.get('formArray'); }

    async onCreateUser() {
        /** Valeurs des diiférents champs du stepper */
        let infoPerso: any = this.formArray.get([0]).value;
        let infoEntreprise: any = this.formArray.get([1]).value;
        let password: any = this.formArray.get([2]).value;

        /** Photo avatar par defaut (premiere lettre du nom et du prenom) */
        let pic: any = "https://eu.ui-avatars.com/api/?name="+this.formArray.get([0]).value.firstName+"+"+this.formArray.get([0]).value.lastName+"+&size=1000";

        /** Permet de gérer l'affichage des spinners */
        this._navigationService.navigationState();
        this.loadingData$ = this._navigationService.loadingData$;
        this.loading$ = this.authService.isLoading$;

        /** Appel au service et de la fonction gérant l'inscription d'un nouvel utilisateur */
        await this.authService.createUser(infoPerso, infoEntreprise, password, pic);
        this.authService.authError$.subscribe((error) => {
            if (!!error) {
                this.authService.openErrorMessage(error.code, 'FERMER');
            }
        });
    }

    /**
     * On destroy
     */
    ngOnDestroy(): void {
        // Unsubscribe from all subscriptions
        this._unsubscribeAll.next();
        this._unsubscribeAll.complete();
    }
}

/**
 * Confirm password validator
 *
 * @param {AbstractControl} control
 * @returns {ValidationErrors | null}
 */
export const confirmPasswordValidator: ValidatorFn = (control: AbstractControl): ValidationErrors | null => {

    if (!control.parent || !control) {
        return null;
    }

    const password = control.parent.get('password');
    const passwordConfirm = control.parent.get('passwordConfirm');

    if (!password || !passwordConfirm) {
        return null;
    }

    if (passwordConfirm.value === '') {
        return null;
    }

    if (password.value === passwordConfirm.value) {
        return null;
    }

    return { 'passwordsNotMatching': true };
};
