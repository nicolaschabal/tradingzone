
import { first } from 'rxjs/operators';
import { Injectable, NgZone } from '@angular/core';
import { AngularFireAuth } from "@angular/fire/auth";
import { AngularFirestore } from '@angular/fire/firestore';
import { Router } from "@angular/router";
import { UserService } from "../../apps/users/user.service";
import { BehaviorSubject } from 'rxjs';
import { MatSnackBar } from '@angular/material';

@Injectable({
  providedIn: 'root'
})
export class AuthentificationService {
  // Permet de gérer le temps de chargement des verifications (mot de passe ou création d'un user)
  private isLoading = new BehaviorSubject<boolean>(false);
  public isLoading$ = this.isLoading.asObservable();

  // Permet de gérer les erreurs : on alimente un observable qui sera retransmis dans les components
  private authError = new BehaviorSubject<firebase.FirebaseError>(null);
  public authError$ = this.authError.asObservable();

  constructor(
    public afs: AngularFirestore,
    public afAuth: AngularFireAuth,
    public router: Router,
    private userService: UserService,
    private _snackBar: MatSnackBar,
    private zone: NgZone
  ) { }

  /**
   * @param email 
   * @param password 
   * @description Firebase verifie si l'émail et le password entré sont corrects, si oui on set le User stocké en base dans le localStorage
   */
  async login(email: string, password: string) {
    this.authError.next(null); // On set l'observable a null
    this.isLoading.next(true); // La page commence à charger ...
    try {
      const userCredential = await this.afAuth.auth.signInWithEmailAndPassword(email, password);
      if (userCredential.user.emailVerified) {
        this.userService.setCurrentUserCredential(userCredential.user); // On set le localStorage
        this.userService.getUser(userCredential.user.uid).subscribe((res) => {
          localStorage.setItem('currentUser', JSON.stringify(res))
          this.userService.setCurrentUser(userCredential.user.uid);// On set le localStorage
          console.log(res);
          this.isLoading.next(false);
          
          this.router.navigate(['apps/calendar/dashboard']);
        });
      } else {
        if (!userCredential.user.emailVerified) {
          console.log('Ton email est pas verifié connard');
          this.router.navigate(['pages/auth/mail-confirm']);
        }
      }
    }
    catch (error) {
      this.isLoading.next(false)
      this.authError.next(error);
      console.error(error);
    }
  }

  /**
   * @param user 
   * @description méthode firebase pour créer une nouvel utilisateur faisant appel à une méthode pour insérer en base
   */
  async createUser(infoPerso, infoEntreprise, password, pic) {
    console.log(infoPerso.email);
    this.isLoading.next(true);
    this.authError.next(null);
    try {
      const userCredential = await this.afAuth.auth.createUserWithEmailAndPassword(infoPerso.email, password.password);
      let newUser = {
        uid: userCredential.user.uid,
        firstName: infoPerso.firstName,
        lastName: infoPerso.lastName,
        email: infoPerso.email,
        birthday: infoPerso.birthday,
        company: infoEntreprise.company,
        phone: infoEntreprise.phone,
        job: infoEntreprise.job,
        isManager: password.isManager,
        picturePath: password.picturePath,
        pictureUrl: pic
      }
      this.sendVerificationMail();
      this.afs.doc(`users/${userCredential.user.uid}`).set(newUser);
      this.isLoading.next(false);
    } catch (error) {
      this.isLoading.next(false);
      this.authError.next(error);
      console.log(error);
    }
  }

  /**
   * @description méthode firebase pour envoyer un email de confirmation
   */
  async sendVerificationMail() {
    await this.afAuth.auth.currentUser.sendEmailVerification();
    this.router.navigate(['pages/auth/mail-confirm']);
  }


  /**
   * @param passwordResetEmail 
   * @description Méthode firebase qui envoie un mail pour recupérer son mdp
   */
  async forgotPassword(passwordResetEmail) {
    this.isLoading.next(true);
    this.authError.next(null);
    try {
      await this.afAuth.auth.sendPasswordResetEmail(passwordResetEmail);
      this.isLoading.next(false);
    } catch (error) {
      this.isLoading.next(false);
      this.authError.next(error);
    }
  }

  /**
   * @description Permet de se deconnecter et de naviguer vers le login
   */
  async signOut() {
    try {
      await this.afAuth.auth.signOut();
      this.userService.clearLocalStorage()
      this.router.navigate(['/pages/auth/login-2']);
    } catch (error) {
      console.error(error);
    }
  }

  /**
   * @description Renvoie une promesse sur l'état de connexion de l'utilsateur
   */
  getUser(): Promise<any> | Promise<firebase.User> {
    return this.afAuth.authState.pipe(first()).toPromise();
  }

  /**
 * @param errorCode 
 * @param action 
 * @description Cette méthode pernd en paramètre un codeError transmis par Firebase,
 * ce code est traduit en erreur, puis on utilise un composant angularMaterial, pour afficher
 * cette erreur dans une 'snackBar'
 */
  public openErrorMessage(errorCode: string, action: string) {
    let error: string
    switch (errorCode) {
      case 'auth/invalid-email':
        error = 'E-mail invalide'
        break;
      case 'auth/user-disabled':
        error = 'Cet utilisateur a été désactivé'
        break;
      case 'auth/user-not-found':
        error = 'Cet utilisateur n\'existe pas'
        break;
      case 'auth/wrong-password':
        error = 'Mot de passe incorrect'
        break;
      case 'auth/too-many-requests':
        error = 'Trop de tentatives échouées'
      case 'auth/email-already-in-use':
        error = 'Cette adresse email est déjà utilisée';
        break;
    }
    this.zone.run(() => {
      this._snackBar.open(error, action, {
        duration: 5000,
        horizontalPosition: 'right',
        verticalPosition: 'top',
      });
    });

  }
}