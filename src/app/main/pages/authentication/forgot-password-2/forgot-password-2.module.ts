import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { MatButtonModule, MatFormFieldModule, MatIconModule, MatInputModule } from '@angular/material';
import {MatProgressSpinnerModule} from '@angular/material/progress-spinner';

import { FuseSharedModule } from '@fuse/shared.module';

import { ForgotPassword2Component } from 'app/main/pages/authentication/forgot-password-2/forgot-password-2.component';
import { SecureInnerPagesGuard } from '../../../guards/secure-inner-pages.guard';

const routes = [
    {
        path     : 'auth/forgot-password-2',
        component: ForgotPassword2Component,
        canActivate: [ SecureInnerPagesGuard ]
    }
];

@NgModule({
    declarations: [
        ForgotPassword2Component
    ],
    imports     : [
        RouterModule.forChild(routes),
        
        MatProgressSpinnerModule,
        MatButtonModule,
        MatFormFieldModule,
        MatIconModule,
        MatInputModule,

        FuseSharedModule,
    ]
})
export class  ForgotPassword2Module
{
}
