import { AuthentificationService } from '../pages/authentication/authentification.service';
import { Injectable } from '@angular/core';
import { CanActivate, ActivatedRouteSnapshot, RouterStateSnapshot, Router } from '@angular/router';
import { Observable } from 'rxjs';
import { FuseConfigService } from '@fuse/services/config.service';

@Injectable({
  providedIn: 'root'  
})
export class AuthGuard implements CanActivate {

  constructor(
    public authService: AuthentificationService,
    public router: Router,
    public fuseConfigService: FuseConfigService
  ) { }

  /**
   * @description Permet d'empêcher d'acceder aux pages internes de l'application 
   * Si l'utilisateur est connecté, si il a validé son mail et si le localStorage n'est pas vide
   * alors on autorise l'accès à la page.
   */
  async canActivate(): Promise<boolean> {
    const user = await this.authService.getUser();
    const  loggedIn = !!(user );
    if (!loggedIn) {
      console.log('Connectez vous afin d\'accéder à cette page');
      
      this.router.navigate(['/pages/auth/login-2']);
      // Cache la sidebar si on est pas connecté
      this.fuseConfigService.config = {
        layout: {
          navbar: {
            hidden: true
          },
          toolbar: {
            hidden: true
          },
          footer: {
            hidden: true
          },
          sidepanel: {
            hidden: true
          }
        }
      };
    }
    return loggedIn;
  }
}
