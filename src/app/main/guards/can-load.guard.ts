import { Injectable } from '@angular/core';
import { CanActivate, ActivatedRouteSnapshot, RouterStateSnapshot, CanLoad, Router } from '@angular/router';
import { Observable } from 'rxjs';
import { FuseConfigService } from '@fuse/services/config.service';
import { AuthentificationService } from '../pages/authentication/authentification.service';

@Injectable({
  providedIn: 'root'
})
export class CanLoadGuard implements CanLoad {
  constructor(
    private fuseConfigService: FuseConfigService,
    private authService: AuthentificationService,
    private router: Router
  ) {}
  async canLoad(route: import("@angular/router").Route, segments: import("@angular/router").UrlSegment[]): Promise<boolean> {
    const user = await this.authService.getUser();
    const loggedIn = !!user;
    if (!loggedIn) { 
      console.log('accès refusé');
      this.router.navigate(['/pages/auth/login-2']);
      // Configure the layout
      this.fuseConfigService.config = {
        layout: {
          navbar: {
            hidden: true
          },
          toolbar: {
            hidden: true
          },
          footer: {
            hidden: true
          },
          sidepanel: {
            hidden: true
          }
        }
      };
    }
      return loggedIn;
  }
}
