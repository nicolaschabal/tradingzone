import { AuthentificationService } from '../pages/authentication/authentification.service';
import { Injectable } from '@angular/core';
import { CanActivate, Router } from '@angular/router';
@Injectable({
  providedIn: 'root'
})
export class SecureInnerPagesGuard implements CanActivate {

  constructor(
    public authService: AuthentificationService,
    public router: Router
  ) { }

  /**
   * @description Permet d'empecher d'accéder aux pages type login/register/forgetPassword... alors  que l'utilisateur est connecté si l'utilisateur n'est pas connecté il y a accès
   */
  async canActivate(): Promise<boolean> {
    const user = await this.authService.getUser(); 
    let userCredential: firebase.User = user
    const loggedIn = !!user;
    const isUserInLocalStorage = !!localStorage.getItem('CurrentUser');
    if ((loggedIn && userCredential.emailVerified)) {
      if(!isUserInLocalStorage) {
        await this.authService.signOut();
      } else {
        console.log('Vous êtes déjà connecté !');
        this.router.navigate(['apps/calendar/dashboard']);
      }
    } 
    return true;
  }
}
